
BOOL_CONVERTER = {
    True: 1,
    False: 2
}

CHAR_CONVERTER = {
    'A': 1,
    'B': 2,
    'C': 3,
    'D': 4,
    'E': 5,
    'F': 6
}