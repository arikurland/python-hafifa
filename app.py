from flask import Flask
import os
from config import CONFIG_DICT
from celery import Celery


conf = CONFIG_DICT['Dev']

app = Flask(__name__)
celery = Celery(app.name, broker=conf.CELERY_BROKER_URL)
celery.conf.result_backend = conf.CELERY_RESULT_BACKEND


from core.resource_manager import ResourceManager
resource_manager = ResourceManager(conf)


@app.route('/')
def hello():
    return "Hello World!"


@app.route('/ipv4/<hostname>')
def get_hostname_ipv4(hostname: str):
    return resource_manager.polling_handler.get_ipv4(host_url=hostname)


@app.route('/check_status/<hostname>/<ipv4>')
def check_status(hostname: str, ipv4: str):
    task = resource_manager.polling_handler.endpoint_to_check.apply_async(args=[hostname, ipv4])
    return task.id, 202


@app.route('/check_progress/<task_id>')
def check_progress(task_id):
    ans = resource_manager.polling_handler.check_enpoint_status(task_id=task_id)
    return ans


