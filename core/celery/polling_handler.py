import requests
from config import BaseConfig
from core.db_handler.db_handler import DBHandler
from app import celery


class PollingHandler:

    def __init__(self, config: BaseConfig, db_handler: DBHandler):
        self.config = config
        self.db_handler = db_handler

    @celery.task(bind=True)
    def endpoint_to_check(self, host_url: str, ipv4):
        params = {'host': host_url, 's': ipv4}

        done_endpoints = 0

        while done_endpoints != len(BaseConfig.EPS_LIST):
            data = requests.get(self.config.GET_EP_STATUS_URL, params=params).json()
            done_endpoints = self.get_finished_endpoints(data)

            self.update_state(state='PROGRESS',
                              meta={'endpoints_to_check': len(BaseConfig.EPS_LIST),
                                     'endpoints_done': done_endpoints})

        return "finished all endpoints checking"

    def get_ipv4(self, host_url: str):
        params = {'host': host_url}
        res = requests.get(self.config.GET_IPv4_URL, params=params).json()

        ipv4 = res['endpoints'][0]['ipAddress']

        self.db_handler.add_endpoint(data={'hostname': host_url,
                                           'ipv4': ipv4})
        return ipv4

    def get_finished_endpoints(self, data):
        endpoints = BaseConfig.EPS_LIST
        counter: int = 0

        for ep in endpoints:
            if ep in data['details'] or ep in data:
                counter += 1

        print("finished " + str(counter) + " out of " + str(len(endpoints)))

        return counter

    def check_enpoint_status(self, task_id):

        task = self.polling_handler.endpoint_to_check.AsyncResult(task_id=task_id)

        if task.state == 'PENDING':
            print("didnt start yet")
        elif task.state != 'FAILURE':
            print(task.state)
        else:
            print('TASK FAILED')

        return task.state

