from core.db_handler import Session
from core.tables.endpoints import Endpoints
from core.tables.host_to_ipv4 import HostToIPv4


class DBHandler:

    def add_endpoint(self, data):
        session = Session()

        new_item = HostToIPv4(ipv4=data['ipv4'], hostname=data['hostname'])
        session.add(new_item)

        session.commit()
        session.close()

    def update_endpoint(self, data):
        pass

    def all_endpoints(self):
        session = Session()
        all_endpoints = session.query(Endpoints).all()
        return all_endpoints