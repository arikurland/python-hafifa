from config import CONFIG_DICT
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


conf = CONFIG_DICT['Dev']

engine = create_engine(conf.CONNECTION_STRING)
Session = sessionmaker(bind=engine)
