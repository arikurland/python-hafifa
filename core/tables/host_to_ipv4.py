from sqlalchemy import Column, VARCHAR
from core.tables import Base


class HostToIPv4(Base):

    __tablename__ = 'host_to_ipv4'

    hostname = Column(VARCHAR(50), primary_key=True)
    ipv4 = Column(VARCHAR(13))
