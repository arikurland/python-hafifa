from sqlalchemy import Column, INT, VARCHAR
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class SomeClass(Base):
    __tablename__ = 'temp_tbl'
    ad = Column(VARCHAR, primary_key=True)
    name = Column(INT)
