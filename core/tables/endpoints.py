from sqlalchemy import Column, INT, VARCHAR
from core.tables import Base


class Endpoints(Base):

    __tablename__ = 'endpoints'

    id = Column(INT, primary_key=True)
    ipAddress = Column(VARCHAR)
    statusMessage = Column(VARCHAR)
    statusDetails = Column(VARCHAR)
    statusDetailsMessage = Column(VARCHAR)

