from config import BaseConfig
from core.db_handler.db_handler import DBHandler
#from core.progress_checker.progress_checker import ProgressChecker
from core.celery.polling_handler import PollingHandler


class ResourceManager:

    def __init__(self, config: BaseConfig):
        self.polling_handler = PollingHandler(config=config, db_handler=DBHandler())
 #       self.progress_checker = ProgressChecker(PollingHandler(config=config, db_handler=DBHandler()))
