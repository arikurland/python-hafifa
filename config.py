class BaseConfig:
    #CONNECTION_STRING = "jdbc:sqlserver://dominosa.cpnesnkah7xs.us-east-2.rds.amazonaws.com:1433;database=python_hafifa"
    CONNECTION_STRING = "mssql+pymssql://dominosa:dominosa@dominosa.cpnesnkah7xs.us-east-2.rds.amazonaws.com/python_hafifa"
    EPS_LIST = ['grade', 'heartbleed', 'vulnBeast', 'renegSupport', 'poodle', 'freak']
    HOSTNAME = 'facebook.com'
    GET_IPv4_URL = 'https://api.ssllabs.com/api/v3/analyze'
    GET_EP_STATUS_URL = 'https://api.ssllabs.com/api/v3/getEndpointData'
    CELERY_BROKER_URL = 'redis://localhost:6379/0'
    CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'

    

class DevConfig(BaseConfig):
    pass


class TestConfig(BaseConfig):
    pass


class ProdConfig(BaseConfig):
    pass


CONFIG_DICT = {
    'Dev': DevConfig,
    'Test': TestConfig,
    'Prod': ProdConfig
}
